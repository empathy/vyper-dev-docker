#!/bin/bash

source config

# note that it's possible to pass extra arguments to the build script, e.g. to
# build the image from scratch without using cache
docker build -t $USERNAME/$IMG_NAME-integration-tests --target integration-tests $1 .
