import json
import web3

from web3 import Web3, TestRPCProvider
from vyper import compiler
from web3.contract import ConciseContract

# Vyper source code
with open("greeter.vy") as contract_file:
   contract_source_code = contract_file.read()

# Compiled source code
abi = json.dumps(compiler.mk_full_signature(contract_source_code))
bytecode = "0x" + compiler.compile(contract_source_code).hex()

# web3.py instance
w3 = Web3(TestRPCProvider())

# Instantiate and deploy contract
contract = w3.eth.contract(abi=abi, bytecode=bytecode)

# Get transaction hash from deployed contract
tx_hash = contract.deploy(transaction={"from": w3.eth.accounts[0], "gas": 410000})

# Get tx receipt to get contract address
tx_receipt = w3.eth.getTransactionReceipt(tx_hash)
contract_address = tx_receipt["contractAddress"]

# Contract instance in concise mode
contract_instance = w3.eth.contract(address=contract_address, abi=abi,ContractFactoryClass=ConciseContract)

# Getters + Setters for web3.eth.contract object
initial_value = contract_instance.greet()
print("Initial value: {}".format(initial_value))
assert initial_value == b"Hello"

set_value = b"Nihao"
contract_instance.setGreeting(set_value, transact={"from": w3.eth.accounts[0]})
print("Setting value to: {}".format(set_value))

final_value = contract_instance.greet()
print("Final value: {}".format(final_value))
assert final_value == set_value
