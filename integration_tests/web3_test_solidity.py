import web3

from web3 import Web3, TestRPCProvider
from solc import compile_source
from web3.contract import ConciseContract

# Solidity source code
with open("greeter.sol") as contract_file:
   contract_source_code = contract_file.read()

# Compiled source code
contract_interface = compile_source(contract_source_code)["<stdin>:Greeter"]
abi = contract_interface["abi"]
bytecode = contract_interface["bin"]

# web3.py instance
w3 = Web3(TestRPCProvider())

# Instantiate and deploy contract
contract = w3.eth.contract(abi=abi, bytecode=bytecode)

# Get transaction hash from deployed contract
tx_hash = contract.deploy(transaction={"from": w3.eth.accounts[0], "gas": 410000})

# Get tx receipt to get contract address
tx_receipt = w3.eth.getTransactionReceipt(tx_hash)
contract_address = tx_receipt["contractAddress"]

# Contract instance in concise mode
contract_instance = w3.eth.contract(address=contract_address, abi=abi,ContractFactoryClass=ConciseContract)

# Getters + Setters for web3.eth.contract object
initial_value = contract_instance.greet()
print("Initial value: {}".format(initial_value))
assert initial_value == "Hello"

set_value = "Nihao"
contract_instance.setGreeting(set_value, transact={"from": w3.eth.accounts[0]})
print("Setting value to: {}".format(set_value))

final_value = contract_instance.greet()
print("Final value: {}".format(final_value))
assert final_value == set_value
