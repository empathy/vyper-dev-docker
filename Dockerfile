######################
## PRODUCTION IMAGE ##
######################

# Use a Ubuntu 18.04 runtime as a parent image
FROM ubuntu:bionic AS production

# Install dependencies
ARG BUILD_DEPS="python3-dev python3-pip git gcc pkg-config apt-utils libssl-dev software-properties-common python3-software-properties"
ARG RUNTIME_DEPS="python3-setuptools libsecp256k1-dev"
ARG RUNTIME_TOOLS="less vim"

RUN apt-get update && \
    apt-get install -y --no-install-recommends $BUILD_DEPS $RUNTIME_DEPS $RUNTIME_TOOLS

# Install Solidity & Geth
RUN add-apt-repository ppa:ethereum/ethereum
RUN apt-get update && \
    apt-get install -y --no-install-recommends ethereum solc

# Install system-wide Python packages
RUN pip3 install \
  # Runtime type checking in Python
  typeguard \
  # Python wrapper around the solc Solidity compiler
  py-solc \
  # Limited RPC client intended for use with automated testing
  eth-testrpc \
  # Interacting with (production or test) blockchains using Python
  web3

# Install Vyper from master (it's still in development)
RUN pip3 install -e git+https://github.com/ethereum/vyper.git@master#egg=vyper

# Remove build artifacts and tools
RUN rm -rf /var/lib/apt/lists/*
RUN apt-get purge -y --auto-remove $BUILD_DEPS

#######################
## INTEGRATION TESTS ##
#######################

FROM production AS integration-tests

WORKDIR /integration_tests
COPY integration_tests/. .

# Test Solidity
RUN solc --abi --bin -o solidity_target greeter.sol
RUN cat solidity_target/Greeter.abi
RUN cat solidity_target/Greeter.bin

# Test Vyper
RUN vyper -f abi greeter.vy
RUN vyper greeter.vy

# Test web3
RUN python3 web3_test_solidity.py
RUN python3 web3_test_vyper.py
